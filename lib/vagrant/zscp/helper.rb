module Vagrant
  module Zscp
    # This is a helper that abstracts out the functionality of scping
    # folders so that it can be called from anywhere.
    class ZscpHelper
      def self.cleanup(machine, ssh_info, opts)
        guestpath = opts[:guestpath]
        machine.ui.info("Removing remote #{guestpath}")
        remotely_execute(ssh_info, "sudo rm -rf #{guestpath};")
      end

      def self.scp(machine, ssh_info, opts)
        guestpath = opts[:guestpath]
        guestpath += "/" if !guestpath.end_with?("/")
        hostpath  = File.expand_path(opts[:hostpath], machine.env.root_path)
        hostpath  = Vagrant::Util::Platform.fs_real_path(hostpath).to_s
        hostpath += "/" if !hostpath.end_with?("/")

        if (opts[:zscp_include])
          included_files = opts[:zscp_include].join(' ')
        else
          included_files = '.'
        end

        username = ssh_info[:username]
        host     = ssh_info[:host]

        machine.ui.info("Sending #{hostpath} to #{guestpath}")
        remotely_execute(ssh_info, "sudo mkdir -p #{guestpath} 2> /dev/null; sudo chown #{username}:$(id -gn #{username}) #{guestpath}")

        temp_file = `mktemp -t 'temp.tar.gz' 2> /dev/null || mktemp -t 'temp.tar.gz.XXXX'`.strip
        temp_file_name = Pathname.new(temp_file).basename
        machine.ui.info("Compressing #{hostpath} into #{temp_file}")
        `tar -czhf #{temp_file} -C #{hostpath} #{included_files}`

        machine.ui.info("Copying #{temp_file} to #{guestpath}")
        command = [
          "scp",
          '-o StrictHostKeyChecking=no',
          '-o LogLevel=quiet',
          '-o UserKnownHostsFile=/dev/null',
          "-o port=#{ssh_info[:port]}",
          "-i '#{ssh_info[:private_key_path][0]}'",
          "#{temp_file}",
          "#{username}@#{host}:#{guestpath}"
        ].join(' ')
        `#{command}`

        remotely_execute(ssh_info, "tar -xzf #{guestpath}#{temp_file_name} -C #{guestpath}; rm #{guestpath}#{temp_file_name}")
        machine.ui.info("#{guestpath} synchronised")
      end

      def self.remotely_execute(ssh_info, command)
        ssh_command = [
          'ssh',
          '-o StrictHostKeyChecking=no',
          '-o LogLevel=quiet',
          '-o UserKnownHostsFile=/dev/null',
          "-o port=#{ssh_info[:port]}",
          "-i '#{ssh_info[:private_key_path][0]}'",
          "#{ssh_info[:username]}@#{ssh_info[:host]}",
          "'#{command}'"
        ].join(' ')
        `#{ssh_command}`
      end
    end
  end
end
