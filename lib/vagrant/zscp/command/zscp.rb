require 'optparse'

require "vagrant/action/builtin/mixin_synced_folders"
require_relative "../helper"

module Vagrant
  module Zscp
    module Command
      class Rsync < Vagrant.plugin("2", :command)
        include Vagrant::Action::Builtin::MixinSyncedFolders

        def execute
          opts = OptionParser.new do |o|
            o.banner = "Usage: vagrant zscp [vm-name]"
            o.separator ""
          end

          # Parse the options and return if we don't have any target.
          argv = parse_options(opts)
          return if !argv

          # Go through each machine and perform the rsync
          error = false
          with_target_vms(argv) do |machine|
            if machine.provider.capability?(:proxy_machine)
              proxy = machine.provider.capability(:proxy_machine)
              if proxy
                machine.ui.warn("vagrant.rsync_proxy_machine")
                machine = proxy
              end
            end

            if !machine.communicate.ready?
              machine.ui.error("vagrant.rsync_communicator_not_ready")
              error = true
              next
            end

            # Determine the rsync synced folders for this machine
            folders = synced_folders(machine, cached: true)[:zscp]
            next if !folders || folders.empty?

            # Get the SSH info for this machine so we can access it
            ssh_info = machine.ssh_info

            # Clean up every synced folder first
            foreach_folder(folders) do |folder_opts|
              ZscpHelper.cleanup(machine, ssh_info, folder_opts)
            end

            # Clean up every synced folder first
            foreach_folder(folders) do |folder_opts|
              ZscpHelper.scp(machine, ssh_info, folder_opts)
            end
          end

          return error ? 1 : 0
        end

        def foreach_folder(folders, &block)
          sync_threads = []
          folders.each do |id, folder_opts|
            sync_threads << Thread.new(folder_opts) do |folder_opts|
              block.call(folder_opts)
            end
          end
          sync_threads.each do |t|
            t.join
          end
        end
      end
    end
  end
end
