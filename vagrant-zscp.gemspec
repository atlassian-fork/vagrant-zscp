# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vagrant/zscp/version'

Gem::Specification.new do |spec|
  spec.name          = "vagrant-zscp"
  spec.version       = Vagrant::Zscp::VERSION
  spec.authors       = ["Colin Hebert"]
  spec.email         = ["chebert@atlassian.com"]

  spec.summary       = %q{Copies files to a vagrant VM using scp.}
  spec.description   = %q{Copies files to a vagrant VM using scp and compressing the content.}
  spec.homepage      = "https://www.atlassian.com"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "gem-release", "~> 0.7.3"
  spec.add_runtime_dependency "log4r", "~> 1.1"
end
